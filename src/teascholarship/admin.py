from django.contrib import admin
from .models import *
from .TeaConf import *

class Tea1Inline(admin.TabularInline):
	model = Tea1
	verbose_name = ITEM1

class Tea2Inline(admin.TabularInline):
	model = Tea2
	verbose_name = ITEM2

class Tea3Inline(admin.TabularInline):
	model = Tea3
	verbose_name = ITEM3

class Tea4aInline(admin.TabularInline):
	model = Tea4a
	verbose_name = ITEM4

class Tea4bInline(admin.TabularInline):
	model = Tea4b
	verbose_name = ITEM4

class Tea5aInline(admin.TabularInline):
	model = Tea5a
	verbose_name = ITEM5

class Tea5bInline(admin.TabularInline):
	model = Tea5b
	verbose_name = ITEM5

class Tea7aInline(admin.TabularInline):
	model = Tea7a
	verbose_name = ITEM7

class Tea7bInline(admin.TabularInline):
	model = Tea7b
	verbose_name = ITEM7

@admin.register(TeaSch)
class TeaSch(admin.ModelAdmin):
	inlines = [
		Tea1Inline,
		Tea2Inline,
		Tea3Inline,
		Tea4aInline,
		Tea4bInline,
		Tea5aInline,
		Tea5bInline,
		Tea7aInline,
		Tea7bInline,
	]