from django import forms
from django.core import validators

class UploadForm(forms.Form):
	sName = forms.CharField(label = '檔案名稱', max_length = 128)
	sDescription = forms.CharField(label = '檔案敘述', max_length = 128)
	file = forms.FileField(label = '檔案上傳', help_text = '必須上傳 pdf, doc, docx, png, jpg 等檔案', validators = [validators.FileExtensionValidator(['pdf', 'doc', 'docx', 'png', 'jpg'])], widget=forms.ClearableFileInput(attrs={'multiple': True}))
	