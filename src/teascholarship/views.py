from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from .models import *
from . import teaForm
from django.contrib.auth.decorators import login_required
from .TeaConf import *
from .teaForm import *
from FileUpload.models import FileUpload

def getItemContent(which, num):
	sItemName = DICTTEA['ITEM%s' % (which)]
	sDescription = DICTTEA['DETAIL%s' % (which)]
	sNote = ''
	if ('NOTE%s' % (which)) in DICTTEA:
		sNote = DICTTEA['NOTE%s' % (which)]
	listItems = [DICTTEA['ITEM%s_%s' % (which, i)] for i in range(1,num+1)]
	return (sItemName, sDescription, sNote, listItems)


def getNumOfItems(order):
	if order == '1':
		return 1
	elif order == '2':
		return 2
	elif order == '3':
		return 2
	elif order == '4':
		return 4
	elif order == '5':
		return 3
	elif order == '7':
		return 2

def getTeaItem(order, user):
	teaA = None
	teaB = None
	userTea = TeaSch.objects.get(iAccount = user)
	if order == '1':
		teaA = Tea1.objects.get_or_create(whichTea = userTea)
	elif order == '2':
		teaA = Tea2.objects.get_or_create(whichTea = userTea)
	elif order == '3':
		teaA = Tea3.objects.get_or_create(whichTea = userTea)
	elif order == '4':
		teaA = Tea4a.objects.get_or_create(whichTea = userTea)
		teaB = Tea4b.objects.get_or_create(whichTea = userTea)
	elif order == '5':
		teaA = Tea5a.objects.get_or_create(whichTea = userTea)
		teaB = Tea5b.objects.get_or_create(whichTea = userTea)
	elif order == '7':
		teaA = Tea7a.objects.get_or_create(whichTea = userTea)
		teaB = Tea7b.objects.get_or_create(whichTea = userTea)
	return teaA, teaB

def getTeaCheckPeriod(order):
	yearly, up, down = False, False, False
	if order == '1':
		up = True
	elif order == '2':
		yearly = True
	elif order == '3':
		yearly = True
	elif order == '4':
		up = True
		down = True
	elif order == '5':
		up = True
		down = True		
	elif order == '7':
		up = True
		down = True		
	return yearly, up, down

def getItemChoice(order):
	return 'T%s' % order

def getStatus(order, listItems, tea, when):
	result = []
	if order == '1':
		result.append([listItems[0], Tea1.objects.get(whichTea = tea).get_sStatus_display, '1'])
	elif order == '2':
		result.append([listItems[0], Tea2.objects.get(whichTea = tea).get_sStatus_1_display, '2_1'])
		result.append([listItems[1], Tea2.objects.get(whichTea = tea).get_sStatus_2_display, '2_2'])
	elif order == '3':
		result.append([listItems[0], Tea3.objects.get(whichTea = tea).get_sStatus_1_display, '3_1'])
		result.append([listItems[1], Tea3.objects.get(whichTea = tea).get_sStatus_2_display, '3_2'])
	elif order == '4':
		if when == 'a':
			result.append([listItems[0], Tea4a.objects.get(whichTea = tea).get_sStatus_1_display, '4a_1'])
			result.append([listItems[1], Tea4a.objects.get(whichTea = tea).get_sStatus_2_display, '4a_2'])
			result.append([listItems[2], Tea4a.objects.get(whichTea = tea).get_sStatus_3_display, '4a_3'])
			result.append([listItems[3], Tea4a.objects.get(whichTea = tea).get_sStatus_4_display, '4a_4'])
		elif when == 'b':
			result.append([listItems[0], Tea4b.objects.get(whichTea = tea).get_sStatus_1_display, '4b_1'])
			result.append([listItems[1], Tea4b.objects.get(whichTea = tea).get_sStatus_2_display, '4b_2'])
			result.append([listItems[2], Tea4b.objects.get(whichTea = tea).get_sStatus_3_display, '4b_3'])
			result.append([listItems[3], Tea4b.objects.get(whichTea = tea).get_sStatus_4_display, '4b_4'])
	elif order == '5':
		if when == 'a':
			result.append([listItems[0], Tea5a.objects.get(whichTea = tea).get_sStatus_1_display, '5a_1'])
			result.append([listItems[1], Tea5a.objects.get(whichTea = tea).get_sStatus_2_display, '5a_2'])
			result.append([listItems[2], Tea5a.objects.get(whichTea = tea).get_sStatus_3_display, '5a_3'])
		else:
			result.append([listItems[0], Tea5b.objects.get(whichTea = tea).get_sStatus_1_display, '5b_1'])
			result.append([listItems[1], Tea5b.objects.get(whichTea = tea).get_sStatus_2_display, '5b_2'])
			result.append([listItems[2], Tea5b.objects.get(whichTea = tea).get_sStatus_3_display, '5b_3'])
	elif order == '7':
		if when == 'a':
			result.append([listItems[0], Tea7a.objects.get(whichTea = tea).get_sStatus_1_display, '7a_1'])
			result.append([listItems[1], Tea7a.objects.get(whichTea = tea).get_sStatus_2_display, '7a_2'])	
		else:
			del(listItems[0])
			result.append([listItems[0], Tea7b.objects.get(whichTea = tea).get_sStatus_2_display, '7b_2'])
	return result

def setStatus(tea, whichStatus, status):
	myTea=''
	if whichStatus == '1':
		myTea = Tea1.objects.get(whichTea = tea)
		myTea.sStatus = status
	elif whichStatus == '2_1':
		myTea = Tea2.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '2_2':
		myTea = Tea2.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '3_1':
		myTea = Tea3.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '3_2':
		myTea = Tea3.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '4a_1':
		myTea = Tea4a.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '4a_2':
		myTea = Tea4a.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '4a_3':
		myTea = Tea4a.objects.get(whichTea = tea)
		myTea.sStatus_3 = status
	elif whichStatus == '4a_4':
		myTea = Tea4a.objects.get(whichTea = tea)
		myTea.sStatus_4 = status
	elif whichStatus == '4b_1':
		myTea = Tea4b.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '4b_2':
		myTea = Tea4b.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '4b_3':
		myTea = Tea4b.objects.get(whichTea = tea)
		myTea.sStatus_3 = status
	elif whichStatus == '4b_4':
		myTea = Tea4b.objects.get(whichTea = tea)
		myTea.sStatus_4 = status
	elif whichStatus == '5a_1':
		myTea = Tea5a.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '5a_2':
		myTea = Tea5a.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '5a_3':
		myTea = Tea5a.objects.get(whichTea = tea)
		myTea.sStatus_3 = status
	elif whichStatus == '5b_1':
		myTea = Tea5b.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '5b_2':
		myTea = Tea5b.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '5b_3':
		myTea = Tea5b.objects.get(whichTea = tea)
		myTea.sStatus_3 = status
	elif whichStatus == '7a_1':
		myTea = Tea7a.objects.get(whichTea = tea)
		myTea.sStatus_1 = status
	elif whichStatus == '7a_2':
		myTea = Tea7a.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	elif whichStatus == '7b_2':
		myTea = Tea7b.objects.get(whichTea = tea)
		myTea.sStatus_2 = status
	myTea.save()

def checkTea(user):
	isTea = False
	if len(user.groups.filter(name = '師獎生')) != 0:
		isTea = True
	return isTea

@login_required
def tea(request, order='1'):
	if order not in ['1','2','3','4','5','7']:
		return HttpResponse("Wrong number!")
	user = request.user
	if checkTea(user) != True:
		return HttpResponse("您不是師獎生；若有誤請聯絡～～～<a href=\"/user\">回個人頁面</a>")
	userTea, isNew = TeaSch.objects.get_or_create(iAccount = user)
	numOfItems = getNumOfItems(order)
	sItemName, sDescription, sNote, listItems = getItemContent(order, numOfItems)
	teaA, teaB = getTeaItem(order, user)
	yearly, up, down = getTeaCheckPeriod(order)

	fileList = FileUpload.objects.filter(iAccount = user, item = getItemChoice(order), sYear = SEMESTER)

	items = []
	items_a = []
	items_b = []
	if yearly:
		items = getStatus(order, listItems, userTea, '')
	if up:
		items_a = getStatus(order, listItems, userTea, 'a')
	if down:
		items_b = getStatus(order, listItems, userTea, 'b')

	form = UploadForm(request.POST, request.FILES)
	if request.method == 'POST' and form.is_valid():
		for file in request.FILES.getlist('file'):
			FileUpload.objects.create(iAccount = user, 
										item = getItemChoice(order), 
										sDirectory = '師獎生', 
										file = file, 
										sName = request.POST.get('sName'), 
										sDescription = request.POST.get('sDescription'), 
										sYear = SEMESTER
									)

	if request.method == 'POST' and request.POST.getlist('checkedfiles') != None:
		for pk in request.POST.getlist('checkedfiles'):
			todelete = FileUpload.objects.get(pk = pk)
			todelete.delete()

	return render(request, 'teascholarship/tea_item1.html', locals())

@login_required
def checkout_someone(request, student, order):
	if request.user.is_staff != True:
		return HttpResponse("您非管理員。<a href=\"/user\">回個人頁面</a>")	
	if order not in ['1','2','3','4','5','7']:
		return HttpResponse("Wrong number!<a href=\"/user\">回個人頁面</a>")
	user = User.objects.get(username = student)
	if checkTea(user) != True:
		return HttpResponse("此帳號非師獎生。<a href=\"/user\">回個人頁面</a>")
	userTea, isNew = TeaSch.objects.get_or_create(iAccount = user)
	numOfItems = getNumOfItems(order)
	sItemName, sDescription, sNote, listItems = getItemContent(order, numOfItems)
	teaA, teaB = getTeaItem(order, user)
	yearly, up, down = getTeaCheckPeriod(order)

	fileList = FileUpload.objects.filter(iAccount = user, item = getItemChoice(order), sYear = SEMESTER)

	items = []
	items_a = []
	items_b = []
	if yearly:
		items = getStatus(order, listItems, userTea, '')
	if up:
		items_a = getStatus(order, listItems, userTea, 'a')
	if down:
		items_b = getStatus(order, listItems, userTea, 'b')


	if request.method == 'POST' and request.POST.getlist('selectedItem') != []:
		status = request.POST.get('itemStatus')
		for item in request.POST.getlist('selectedItem'):
			setStatus(userTea, item, status)
		return redirect('/tea/checkout/someone/%s/%s' %(student, order))

	if request.method == 'POST' and request.POST.getlist('suggestion') != []:
		for i in range(len(fileList)):
			file = fileList[i]
			suggestion = request.POST.getlist('suggestion')[i]
			file.sSuggestion = suggestion
			file.save()
		return redirect('/tea/checkout/someone/%s/%s' %(student, order))

	if yearly and request.method == 'POST' and (request.POST.get('the_suggestion') != teaA[0].sSuggestion):
		teaA[0].sSuggestion = request.POST.get('the_suggestion')
		teaA[0].save()
		return redirect('/tea/checkout/someone/%s/%s' %(student, order))

	if up and (request.method == 'POST') and (request.POST.get('up_suggestion') != teaA[0].sSuggestion):
		teaA[0].sSuggestion = request.POST.get('up_suggestion')
		teaA[0].save()
		return redirect('/tea/checkout/someone/%s/%s' %(student, order))
	if down and request.method == 'POST' and (request.POST.get('down_suggestion') != teaB[0].sSuggestion):
		teaB[0].sSuggestion = request.POST.get('down_suggestion')
		teaB[0].save()
		return redirect('/tea/checkout/someone/%s/%s' %(student, order))

	return render(request, 'teascholarship/tea_checkout1.html', locals())

@login_required
def checkout_list(request):
	if request.user.is_staff != True:
		return HttpResponse("您非管理員。<a href=\"/user\">回個人頁面</a>")	
	group = Group.objects.get(name = '師獎生')
	teaList = group.user_set.all()
	return render(request, 'teascholarship/tea_checkout_list.html', locals())

def getSuggestion(order, tea):
	suggestion = ''
	if order == '1':
		suggestion = Tea1.objects.get(whichTea = tea).sSuggestion
	elif order == '2':
		suggestion = Tea2.objects.get(whichTea = tea).sSuggestion
	elif order == '3':
		suggestion = Tea3.objects.get(whichTea = tea).sSuggestion
	elif order == '4a':
		suggestion = Tea4a.objects.get(whichTea = tea).sSuggestion
	elif order == '4b':
		suggestion = Tea4b.objects.get(whichTea = tea).sSuggestion
	elif order == '5a':
		suggestion = Tea5a.objects.get(whichTea = tea).sSuggestion
	elif order == '5b':
		suggestion = Tea5b.objects.get(whichTea = tea).sSuggestion
	elif order == '7a':
		suggestion = Tea7a.objects.get(whichTea = tea).sSuggestion
	elif order == '7b':
		suggestion = Tea7b.objects.get(whichTea = tea).sSuggestion
	return suggestion

def getFile(order, user):
	fileList = FileUpload.objects.filter(iAccount = user, item = getItemChoice(order), sYear = SEMESTER)
	resultList = []
	for file in fileList:
		resultList.append([file.file.url, file.sName, file.sDescription, file.sSuggestion])
	return resultList

@login_required
def checkout_by_name_whole_page(request, student='kyle'):
	if request.user.is_staff != True:
		return HttpResponse("您非管理員。<a href=\"/user\">回個人頁面</a>")
	student = User.objects.get(username = student)
	tea = TeaSch.objects.get(iAccount = student)

	suggestion1 = getSuggestion('1', tea)


	fileList1 = getFile('1', student)

	status1 = ['項目名稱','項目狀態']
	statusList1 = [status1,['a','b']]

	item1 = [
				ITEM1,
				statusList1,
				fileList1,
				suggestion1,
	]
	itemList = [
				item1,
	]

	return render(request, 'teascholarship/tea_checkout.html', locals())







