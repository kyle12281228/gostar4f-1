from django.db import models
from django.contrib.auth.models import User
import uuid
from .TeaConf import *

PASS = 'PS'
INPROCESS = 'IP'
NOPASS = 'NP'
NOTUPLOAD = 'NU'
choices = [
	(PASS, '通過'),
	(INPROCESS, '待審核'),
	(NOPASS, '不通過'),
	(NOTUPLOAD, '未上傳'),
	]

class TeaSch(models.Model):
	sYear = models.CharField(verbose_name = '學期', default = SEMESTER, max_length = 8)
	iAccount = models.ForeignKey(User, verbose_name = '師獎生', on_delete = models.CASCADE)
	idOfTea = models.CharField(verbose_name = '編號', default = uuid.uuid4, editable = False, max_length = 36)

	def __str__(self):
		return '%s 的師獎生 %s 年度檢核表' % (self.iAccount, self.sYear)


# 1.學業成績 - 共1項須上傳 - 只有一個檔案
class Tea1(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus = models.CharField(verbose_name = ITEM1+ITEM1_1, choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '1.學業成績-總審查意見', blank = True, null = True, max_length = 128)

	def __str__(self):
		return '%s - %s' % (self.whichTea, ITEM1)

# 2.教學實務能力檢測 - 共2項須上傳
class Tea2(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = ITEM2+'-'+ITEM2_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = ITEM2+'-'+ITEM2_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = ITEM2+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s' % (self.whichTea, ITEM2)

# 3.服務學習 - 共2項須上傳
class Tea3(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = ITEM3+'-'+ITEM3_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = ITEM3+'-'+ITEM3_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = ITEM3+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s' % (self.whichTea, ITEM3)

# 4a.義務輔導實數-上學期 - 共4項須上傳
class Tea4a(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = '上學期-'+ITEM4+'-'+ITEM4_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = '上學期-'+ITEM4+'-'+ITEM4_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_3 = models.CharField(verbose_name = '上學期-'+ITEM4+'-'+ITEM4_3+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_4 = models.CharField(verbose_name = '上學期-'+ITEM4+'-'+ITEM4_4+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '上學期-'+ITEM4+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 上學期' % (self.whichTea, ITEM4)

# 4b.義務輔導實數-下學期 - 共4項須上傳
class Tea4b(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = '下學期-'+ITEM4+'-'+ITEM4_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = '下學期-'+ITEM4+'-'+ITEM4_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_3 = models.CharField(verbose_name = '下學期-'+ITEM4+'-'+ITEM4_3+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_4 = models.CharField(verbose_name = '下學期-'+ITEM4+'-'+ITEM4_4+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '下學期-'+ITEM4+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 下學期' % (self.whichTea, ITEM4)

# 5a.校內外工作坊/研習-上學期 - 共3項須上傳
class Tea5a(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = '上學期-'+ITEM5+'-'+ITEM5_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = '上學期-'+ITEM5+'-'+ITEM5_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_3 = models.CharField(verbose_name = '上學期-'+ITEM5+'-'+ITEM5_3+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '上學期-'+ITEM5+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 上學期' % (self.whichTea, ITEM5)

# 5b.校內外工作坊/研習-下學期 - 共3項須上傳
class Tea5b(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = '下學期-'+ITEM5+'-'+ITEM5_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = '下學期-'+ITEM5+'-'+ITEM5_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_3 = models.CharField(verbose_name = '下學期-'+ITEM5+'-'+ITEM5_3+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '下學期-'+ITEM5+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 下學期' % (self.whichTea, ITEM5)

# 7a.填報個人學習及服務歷程檔案-上學期 - 共1項須上傳
class Tea7a(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)
	sStatus_1 = models.CharField(verbose_name = '上學期-'+ITEM7+'-'+ITEM7_1+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sStatus_2 = models.CharField(verbose_name = '上學期-'+ITEM7+'-'+ITEM7_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '上學期-'+ITEM7+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 上學期' % (self.whichTea, ITEM7)

# 7b.填報個人學習及服務歷程檔案-下學期 - 共1項須上傳
class Tea7b(models.Model):
	whichTea = models.OneToOneField(TeaSch, verbose_name = '年份及人', on_delete = models.CASCADE)

	sStatus_2 = models.CharField(verbose_name = '下學期-'+ITEM7+'-'+ITEM7_2+'-審查狀態', choices = choices, default = 'NU', max_length = 2)
	sSuggestion = models.CharField(verbose_name = '下學期-'+ITEM7+'-總審查意見', null = True, blank = True, max_length = 128)

	def __str__(self):
		return '%s - %s - 下學期' % (self.whichTea, ITEM7)

