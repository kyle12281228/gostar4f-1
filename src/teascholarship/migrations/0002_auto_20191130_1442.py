# Generated by Django 2.2.6 on 2019-11-30 06:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teascholarship', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tea7',
            new_name='Tea7b',
        ),
        migrations.CreateModel(
            name='Tea7a',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sStatus', models.CharField(choices=[('PS', '通過'), ('IP', '待審核'), ('NP', '不通過'), ('NU', '未上傳')], default='NU', max_length=2, verbose_name='上學期-7.填報個人學習及服務歷程檔案-上傳強化師生教育實習歷程平台-審查狀態')),
                ('sSuggestion', models.CharField(blank=True, max_length=128, verbose_name='上學期-7.填報個人學習及服務歷程檔案-總審查意見')),
                ('whichTea', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='teascholarship.TeaSch', verbose_name='年份及人')),
            ],
        ),
    ]
