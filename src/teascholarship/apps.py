from django.apps import AppConfig


class TeascholarshipConfig(AppConfig):
    name = 'teascholarship'
