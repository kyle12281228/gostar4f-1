from django.db import models
from django.contrib.auth.models import User
import uuid
import os
from django.dispatch import receiver

"""
class FileUpload:
	iAccount : 某個人的檔案
	item : 審查項目（六項）、或是作品、其他
	file : 檔案, 上傳的位置及檔名如 user_directory_path 所定義
	sName : 檔案名稱（與實際上檔案名稱不同，此為提供辨識）
	sDescription : 檔案敘述
	sSuggestion : 審查意見
	sYear : 學期，例如1081表示 108年度上學期；1082表108學年度下學期

	此model適用其他檔案上傳，不需要的欄位留空即可。例如審查意見。
"""



def user_directory_path(instance, filename):
	# ext : 副檔名
	ext = filename.split('.')[-1]
	# 用uuid重新命名
	filename = '{}.{}'.format(uuid.uuid4().hex, ext)
	# 例如 /410431135/師獎生/1081/1_學業成績/81f3f363de804da18e0bb6aac433ed0a.pdf
	path = os.path.join(instance.iAccount.username, instance.sDirectory, instance.sYear, instance.item, filename)
	return path


class FileUpload(models.Model):
	TEASCHOOLAR1 = 'T1'
	TEASCHOOLAR2 = 'T2'
	TEASCHOOLAR3 = 'T3'
	TEASCHOOLAR4 = 'T4'
	TEASCHOOLAR5 = 'T5'
	TEASCHOOLAR7 = 'T7'
	OTHERS = 'OT'
	itemChoice = [
		(TEASCHOOLAR1, '1_學業成績'),
		(TEASCHOOLAR2, '2_教學實務能力檢測'),
		(TEASCHOOLAR3, '3_服務學習'),
		(TEASCHOOLAR4, '4_義務輔導時數'),
		(TEASCHOOLAR5, '5_校內外工作坊/研習'),
		(TEASCHOOLAR7, '7_填報個人學習及服務歷程檔案'),
		(OTHERS, '其他'),
	]

	iAccount = models.ForeignKey(User, on_delete = models.SET_NULL, null = True)
	item = models.CharField(verbose_name = '項目', max_length = 2, choices = itemChoice, default = 'OT')
	sDirectory = models.CharField(verbose_name = '資料夾', max_length = 32, default = '其他')
	file = models.FileField(verbose_name = '檔案', upload_to = user_directory_path, null = True, blank = True)
	sName = models.CharField(verbose_name = '名稱', null = True, blank = True, max_length = 128)
	sDescription = models.CharField(verbose_name = '敘述', null = True, blank = True, max_length = 128)
	sSuggestion = models.CharField(verbose_name = '審查意見', null = True, blank = True, max_length = 128)
	sYear = models.CharField(verbose_name = '學期', default = 'unclassified', max_length = 12)

	def __str__(self):
		name = '%s 的 %s' % (self.iAccount, self.sName)
		return name


@receiver(models.signals.post_delete, sender=FileUpload)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)



