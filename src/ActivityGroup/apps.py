from django.apps import AppConfig


class ActivitygroupConfig(AppConfig):
    name = 'ActivityGroup'
