# Generated by Django 2.2.6 on 2019-11-01 13:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('article', '0003_auto_20191029_1601'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityGroup',
            fields=[
                ('idOfGroup', models.CharField(default=uuid.uuid4, editable=False, max_length=36, primary_key=True, serialize=False, verbose_name='活動群組編號')),
                ('sGroupTitle', models.CharField(max_length=128, unique=True, verbose_name='活動群組名稱')),
                ('sDescription', models.TextField(blank=True, verbose_name='活動群組描述')),
                ('iStatus', models.SmallIntegerField(default=21, verbose_name='群組狀態')),
                ('createdTime', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='建立時間')),
                ('postTime', models.DateTimeField(blank=True, null=True, verbose_name='發表時間')),
                ('whichBoard', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='article.Board', verbose_name='所屬板塊')),
                ('whoCreate', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='群組創立人')),
            ],
        ),
        migrations.CreateModel(
            name='GroupSubtitle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sSubtitle', models.CharField(max_length=128, verbose_name='子標題')),
                ('sDescription', models.TextField(blank=True, verbose_name='子標題描述')),
                ('whichGroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ActivityGroup.ActivityGroup', verbose_name='所屬活動群組')),
            ],
        ),
        migrations.CreateModel(
            name='UserOfActivityGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('iUserAccount', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='成員')),
                ('idOfGroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ActivityGroup.ActivityGroup', verbose_name='所在活動群組')),
            ],
        ),
        migrations.CreateModel(
            name='has_all_permit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('iUserAccount', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='活動群組管理員')),
                ('idOfGroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ActivityGroup.ActivityGroup', verbose_name='活動群組')),
            ],
        ),
        migrations.CreateModel(
            name='GroupThreeM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('leftTime', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='黏貼時間')),
                ('sContent', models.TextField(verbose_name='便利貼內容')),
                ('whichGroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ActivityGroup.ActivityGroup', verbose_name='所屬活動群組')),
                ('whichSubtitle', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ActivityGroup.GroupSubtitle', verbose_name='所屬子標題')),
                ('whoLeft', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='黏貼的人')),
            ],
        ),
    ]
