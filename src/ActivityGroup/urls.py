from django.urls import path
from . import views

urlpatterns = [
	path('', views.browse, name='browse'),
	path('class=<str:gClass>/search', views.search, name='search group'),
	path('hashtag/<str:sTopic>/', views.hashtag, name='hashtag'),
	path('class=<str:gClass>', views.browse, name='browse_by_status'),
	path('class=True/<str:sGroupTitle>', views.published, name='group_isPublished'),
	path('class=False/<str:sGroupTitle>', views.info, name='view group information'),
	path('class=False/<str:sGroupTitle>/discuss', views.discuss, name='in discussion'),
	path('class=<str:gClass>/<str:sGroupTitle>/discuss/<str:sSubtitle>', views.discuss, name='in sSubtitle_discussion'),
	
]