from django import forms

# 編輯便利貼
class EditThreeMForm(forms.Form):
	sThreeMContent = forms.CharField(label = "便利貼內容", widget = forms.Textarea(attrs={'placeholder':'輸入內容...','autoescape':False}))

# 編輯子標題
class EditSubtitleForm(forms.Form):
	sSubtitle = forms.CharField(label = "子標題", max_length = 128)
	sDescription = forms.CharField(label = "子標題說明", required=False)

# 顯示便利貼+可移動到子標題
class ShowAndSelectThreeMtoSubtitleForm(forms.Form):
	pass

# 即時討論 (later)

# 新增活動群組
status_choices = (
    (0, '公開'),
    (1, '半公開'),
    (2, '不公開'),
)
class AddGroupForm(forms.Form):
	sGroupTitle = forms.CharField(label = "活動群組名稱", widget = forms.TextInput(attrs={'id' : 'title','placeholder':'活動群組名稱'}))
	sDescription = forms.CharField(label = '活動群組描述', required=False, widget = forms.Textarea(attrs={'placeholder':'活動群組描述'}))
	iStatus = forms.ChoiceField(choices=status_choices)
	postTime = forms.DateTimeField(label='發表時間', widget = forms.SelectDateWidget)