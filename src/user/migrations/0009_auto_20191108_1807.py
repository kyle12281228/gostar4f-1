# Generated by Django 2.2.6 on 2019-11-08 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_auto_20191108_1805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinfo',
            name='imageAvatar',
            field=models.ImageField(default='', upload_to='avatar/'),
        ),
    ]
