from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
	path('', views.home, name='home'),
	path('signin_register_tourist/', views.select, name='select'),
	path('user/', views.user, name='user'),
	path('register/', views.register, name='register'),
	path('tourist/', views.tourist, name='tourist'),
	path('login/', views.login, name='login'),
	path('logout/', views.logout, name='logout'),
	path('user/<str:author>/', views.author, name='author'),
]