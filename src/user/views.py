from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import User
from django import forms
from user.forms import RegisterForm, LoginForm, UploadImageForm
from django.http import HttpResponse, HttpResponseRedirect
from user.models import UserInfo
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils import timezone
from article.models import *
from ActivityGroup.models import *
from .models import UserInfo
from django.template.defaultfilters import filesizeformat
from src import settings

BASE_URL_PATH = settings.BASE_URL_PATH

def home(request):
	user = request.user
	userList = User.objects.all()
	return render(request, 'user/user_home.html', locals())

def select(request):
	return render(request, 'user/user_signin_register_tourist.html', locals())

def tourist(request):
	return render(request, 'user/user_tourist.html', locals())

def register(request):
	# upon user send the register information
	if request.method == "POST":
		registerForm = RegisterForm(request.POST)

		errors = []
		if not registerForm.is_valid():
			errors.extend(registerForm.errors.values())
			return render(request, 'user/user_register.html', locals())

		else:
			username = registerForm.cleaned_data['username']
			password1 = registerForm.cleaned_data['password1']
			password2 = registerForm.cleaned_data['password2']
			name = registerForm.cleaned_data['name']
			department = registerForm.cleaned_data['department']
			email = registerForm.cleaned_data['email']
			phone = registerForm.cleaned_data['phone']
			# birthday = registerForm.cleaned_data['birthday']
			gender = registerForm.cleaned_data['gender']

		# verify the length of the password
		if (len(password1) < 8):
			errors.append('密碼太短囉！密碼至少8位~')
			return render(request, 'user/user_register.html', locals())
		# verify if password correct
		if password1 != password2:
			errors.append('輸入的密碼不同！')
			return render(request, 'user/user_register.html', locals())
		# verify if the username is student id (all integer)
		try:
			username = str(int(username))
		except Exception as e:
			errors.append('請使用學號註冊。')
			return render(request, 'user/user_register.html', locals())
		# verify if exist same user
		filterSameUser = User.objects.filter(username = username)
		if (len(filterSameUser) > 0):
			errors.append('使用此學號的使用者已存在。')
			return render(request, 'user/user_register.html', locals())

		# if registeration vaild
		user = User()
		user.username = username
		user.set_password(password1)
		user.email = email
		user.first_name = name[0]
		user.last_name = name[1:]
		user.save()
		
		# verify new user
		newUser = auth.authenticate(username = username, password = password1)
		if newUser is not None:
			auth.login(request, newUser)
			userinfo = UserInfo.objects.create(iAccount = newUser)
			userinfo.iDepartment = department
			userinfo.iPhone = phone
			# userinfo.iBirthday = birthday
			userinfo.iGender = gender
			userinfo.save()
			return HttpResponseRedirect('/user/')

	registerForm = RegisterForm(request.POST)
	return render(request, 'user/user_register.html', locals())


def login(request):
	# placeholder
	# return HttpResponse("login page")

	# if user come from somewhere
	nextValue = request.POST.get('next')
	# if user has already logged in
	if request.user.is_authenticated:
		return HttpResponseRedirect('/board/')
	if request.method == "POST":
		loginForm = LoginForm(request.POST)
		errors =''
		if loginForm.is_valid():
			username = loginForm.cleaned_data['username']
			password = loginForm.cleaned_data['password']
			user = auth.authenticate(username = username, password = password)
			if user and user.is_active:
				auth.login(request, user)
				if nextValue != '':
					return redirect(nextValue)
				else:
					return redirect(BASE_URL_PATH+'board/')
			else:
				errors = "Account or password is wrong."
				# if poassword wrong but from otherwhere
				# this is not a good approach cos redirect twice and no errors message
				if nextValue != '':
					return redirect(nextValue)
				else:
					return render(request, 'user/user_login.html', locals())
	loginForm = LoginForm(request.POST)
	return render(request, 'user/user_login.html', locals())

# this shall not exist, delete someday
def logout(request):
	auth.logout(request)
	return render(request, 'user/user_home.html', locals())

@login_required
def user(request):
	intro = request.POST.get('intro')
	arts = request.POST.get('arts')
	collects = request.POST.get('collects')
	groups = request.POST.get('groups')
	# SHOULD handle excption if user has no userinfo!
	user = request.user
	userList = User.objects.all()
	new, old = UserInfo.objects.get_or_create(iAccount = user)
	userinfo = new or old
	if intro:
		userinfo.sShortIntroduction = intro
		userinfo.save()
		return redirect("/user/")

	if arts:
		if arts == 'open':
			userinfo.bShowMyArts = 'True'
			userinfo.save()
		else:
			userinfo.bShowMyArts = 'False'
			userinfo.save()
		return redirect("/user/")

	if collects:
		if collects == 'open':
			userinfo.bShowMyCollects = 'True'
			userinfo.save()
		else:
			userinfo.bShowMyCollects = 'False'
			userinfo.save()
		return redirect("/user/")

	if groups:
		if groups == 'open':
			userinfo.bShowMyGroups = 'True'
			userinfo.save()
		else:
			userinfo.bShowMyGroups = 'False'
			userinfo.save()
		return redirect("/user/")

	# article that the user posted
	articleList = Article.objects.filter(sAuthor = user, bIsInEdtion = False)
	userinfo.iArticleNumber = len(articleList)
	userinfo.save()

	form = UploadImageForm()
	if request.method == "POST":
		form = UploadImageForm(request.POST, request.FILES)
		if form.is_valid():
			userinfo.imageAvatar.delete()
			userinfo.imageAvatar = form.cleaned_data['imageAvatar']
			userinfo.save()
			return redirect("/user/")
	else:
		form = UploadImageForm()

	userinfo = {
		'user':user,
		'UserName':user.get_full_name(),
		'sNickName':userinfo.sNickName,
		'article_number':userinfo.iArticleNumber,
		'score':userinfo.iScore,
		'read':userinfo.iReadingScore,
		'register':user.date_joined.strftime('%Y-%m-%d'),
		'registerDays':(timezone.now() - user.date_joined).days,
		'offlineDays':(timezone.now() - user.last_login).days,
		'intro':userinfo.sShortIntroduction,
		'bShowMyArts':userinfo.bShowMyArts,
		'bShowMyCollects':userinfo.bShowMyCollects,
		'bShowMyGroups':userinfo.bShowMyGroups,
		}
	

	# 發表的文章
	postedArticles = {'article': articleList}
	# 參與的活動群組
	activityGroups = [group.idOfGroup for group in UserOfActivityGroup.objects.filter(iUserAccount = user)]
	joinedGroups = {'activityGroups': activityGroups}
	# 收藏的文章
	collectArticle = [article.iSerialNumberOfArticle for article in UserCollectArticle.objects.filter(iAccount = user)]

	# 把字典合在一起
	context = {**userinfo, **postedArticles, **joinedGroups, 'collectArticle':collectArticle, 'form':form, "user":user, "userList":userList}
	
	return render(request, 'user/user_user.html', context)

def author(request, author):
	user = request.user
	userList = User.objects.all()
	author = User.objects.get(username = author)

	if author == user:
		return redirect('/user')

	authorinfo = {
		'UserName':author.get_full_name(),
		'sNickName':author.userinfo.sNickName,
		'article_number':author.userinfo.iArticleNumber,
		'score':author.userinfo.iScore,
		'read':author.userinfo.iReadingScore,
		'register':author.date_joined.strftime('%Y-%m-%d'),
		'registerDays':(timezone.now() - author.date_joined).days,
		'offlineDays':(timezone.now() - author.last_login).days,
		'intro':author.userinfo.sShortIntroduction,
		'bShowMyArts':author.userinfo.bShowMyArts,
		'bShowMyCollects':author.userinfo.bShowMyCollects,
		'bShowMyGroups':author.userinfo.bShowMyGroups,
		}
	# 發表的文章
	articleList = Article.objects.filter(sAuthor = author, bIsInEdtion = False)
	postedArticles = {'article': articleList}
	# 參與的活動群組
	activityGroups = [group.idOfGroup for group in UserOfActivityGroup.objects.filter(iUserAccount = author)]
	joinedGroups = {'activityGroups': activityGroups}
	# 收藏的文章
	collectArticle = [article.iSerialNumberOfArticle for article in UserCollectArticle.objects.filter(iAccount = author)]

	# 把字典合在一起
	context = {**authorinfo, **postedArticles, **joinedGroups, 'collectArticle':collectArticle, 'author':author}
	
	return render(request, 'user/user_author.html', context)