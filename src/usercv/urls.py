from django.urls import path
from usercv import views

urlpatterns = [
	path('', views.manage, name='totalcv manage'),
	path('totalcv', views.totalcv, name='totalcv'),
	path('new_mycv', views.new_mycv, name='新增履歷'),
	path('public', views.public, name='公開履歷'),
	path('public/<str:username>/<str:cvid>', views.public_cv, name='公開履歷'),
	path('mycv/<str:cvid>/edit', views.mycv_edit, name = 'set mycv'),
	path('mycv/<str:cvid>/browse', views.mycv_browse, name='browse mycv'),
	path('mycv/<str:cvid>/output', views.mycv_output, name='output mycv'),
]