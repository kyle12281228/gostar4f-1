from django import forms

class TotalCVForm(forms.Form):
	sexChoice = [('M', '男性'), ('F', '女性'), ('O', '其他'), ('N', '不提供')]
	YEARS = [year for year in range(2021, 1911, -1)]

	sName = forms.CharField(label = '名字', required = False, widget = forms.TextInput(attrs={'class' : 'cv_value'}) )
	sNickname = forms.CharField(label = '暱稱', required = False, widget = forms.TextInput(attrs={'class' : 'cv_value'}))
	sex = forms.ChoiceField(label = '性別', choices = sexChoice, required = False, widget=forms.Select(attrs={'class' : 'select'}))
	sPhoneNumber1 = forms.CharField(label = '電話號碼', required = False, widget = forms.TextInput(attrs={'class' : 'cv_value'}))
	sAddressHome = forms.CharField(label = '家裡住址', required = False, widget = forms.TextInput(attrs={'class' : 'cv_value'}))
	sEmail = forms.EmailField(label = '電子信箱', required = False, widget = forms.TextInput(attrs={'class' : 'cv_value'}))
	dateBirthday = forms.DateField(label = '生日', widget = forms.widgets.SelectDateWidget(years = YEARS,attrs={'class' : 'select'}) ,required = False)
	sAutobiography_zh = forms.CharField(label = '中文自傳', required = False, widget = forms.widgets.Textarea(attrs={'class' : 'cv_value'}))
	sAutobiography_en = forms.CharField(label = '英文自傳', required = False, widget = forms.widgets.Textarea(attrs={'class' : 'cv_value'}))