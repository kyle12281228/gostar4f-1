from django.shortcuts import render
from django.http import HttpResponse
from .userImportForm import UploadForm
from FileUpload.models import FileUpload
import csv, os
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

@login_required
def user_import(request):
	user = request.user
	if user.is_staff != True:
		return HttpResponse('您沒有訪問此網頁的權限！')
	form = UploadForm(request.POST, request.FILES)
	rows = []
	if request.method == 'POST' and form.is_valid():
		for file in request.FILES.getlist('file'):
			with file.open(mode='t') as f:
				for line in f.readlines():
					rows.append(line.decode().replace('\r\n', '').replace('\ufeff', '').split(','))

		userList = []
		for username in User.objects.all():
			userList.append(username.username)

		for index in range(len(rows)-1):
			new_user = User()
			new_user.username = rows[index+1][0]
			new_user.set_password(rows[index+1][1])
			new_user.first_name = rows[index+1][2][0]
			new_user.last_name = rows[index+1][2][1:]
			if new_user.username in userList:
				continue
			else:
				new_user.save()
	
	return render(request, 'management/user_import.html', locals())


