from django.urls import path
from . import views

urlpatterns = [
	path('user_import', views.user_import, name='import user'),
]