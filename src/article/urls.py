from django.urls import path
from . import views

app_name = 'article'

urlpatterns = [
	path('', views.board, name='board'),
	path('new_post/', views.new_post, name='new_post'),
	path('<str:sBoardName>/search/', views.search, name='search article'),
	path('hashtag/<str:sTopic>/', views.hashtag, name='hashtag'),
	path('<str:sBoardName>/<str:iSerialNumberOfArticle>/', views.article_detail, name='article detail'),
	path('<str:sBoardName>/', views.board, name='specific board'),
]