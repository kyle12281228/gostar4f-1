from django.contrib import admin

from article.models import *



class CommentInline(admin.TabularInline):
	model = CommentOfArticle
	verbose_name_plural = '留言'

class ArticleInline(admin.TabularInline):
	model = Article
	verbose_name_plural = '文章'

class TopicTagInline(admin.TabularInline):
	model = ArticleOfTopicTag
	verbose_name_plural = '話題標籤'

class CollectionsInline(admin.TabularInline):
	model = UserCollectArticle
	verbose_name_plural = '收藏'

class LikesInline(admin.TabularInline):
	model = UserLikesArticle
	verbose_name_plural = '按讚'

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
	inlines = [
		CommentInline,
		TopicTagInline,
		CollectionsInline,
		LikesInline,
	]

@admin.register(Board)
class BoardAdmin(admin.ModelAdmin):
	inlines = [
		ArticleInline,
	]

@admin.register(TopicTag)
class TopicTag(admin.ModelAdmin):
	pass


class UserLikesCommentInline(admin.TabularInline):
	model = UserLikesComment
	verbose_name_plural = '按讚留言'

@admin.register(CommentOfArticle)
class CommentAdmin(admin.ModelAdmin):
	inlines = [
		UserLikesCommentInline,
	]

	