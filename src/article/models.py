from django.db import models
import uuid
from django.utils import timezone
from django.contrib.auth.models import User

class Board(models.Model):
	idOfBoard = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	sBoardName = models.CharField(verbose_name = '版塊名稱', max_length = 128, unique = True)
	sDescription = models.TextField(verbose_name = '敘述及版規', blank = True)
	
	def __str__(self):
		return '%s' % (self.sBoardName, )

class Article(models.Model):
	sAuthor = models.ForeignKey(User, verbose_name = '作者', on_delete = models.SET_NULL, null = True)
	iSerialNumberOfArticle = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	sTitle = models.CharField(verbose_name = '文章標題', max_length = 128, unique = True)
	sContent = models.TextField(verbose_name = '文章內容')
	# 實作時應存當時時間
	ArticlePostTime = models.DateTimeField(verbose_name = '張貼時間', default = timezone.now)
	# 是否在編輯中(未送出)
	bIsInEdtion = models.BooleanField(default = True)
	# 討論熱度
	iTrending = models.FloatField(default = 0, null = True)
	whichBoard = models.ForeignKey(Board, verbose_name = "所在板塊", on_delete = models.SET_NULL, null = True)
	likeArticleNumber = models.PositiveIntegerField(verbose_name = "按讚數", blank = True, null = True)
	commentNumber = models.PositiveIntegerField(verbose_name = "留言數", blank = True, null = True)

	def __str__(self):
		return '%s by %s' % (self.sTitle, self.sAuthor.get_full_name())

# class BoardOfArticle(models.Model):
# 	idOfBoard = models.ForeignKey(Board, on_delete = models.CASCADE)
# 	iSerialNumberOfArticle = models.ForeignKey(Article, on_delete = models.CASCADE)

class TopicTag(models.Model):
	idOfTopic = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4)
	sTopic = models.CharField(verbose_name = '話題標籤', max_length = 128)

	def __str__(self):
		return self.sTopic

class ArticleOfTopicTag(models.Model):
	iSerialNumberOfArticle = models.ForeignKey(Article, on_delete = models.CASCADE)
	idOfTopic = models.ForeignKey(TopicTag, on_delete = models.CASCADE)

# 使用者留言文章
class CommentOfArticle(models.Model):
	iSerialNumberOfArticle = models.ForeignKey(Article, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.SET_NULL, null = True)
	idOfComment = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	CommentTime = models.DateTimeField(verbose_name = '留言時間', default = timezone.now, editable = False)
	sContentOfComment = models.TextField(verbose_name = '留言內容')
	likeCommentNumber = models.PositiveIntegerField(verbose_name = "按讚數", blank = True, null = True)

	def __str__(self):
		return '%s 留言在 %s' % (self.iAccount, self.iSerialNumberOfArticle)

# 使用者收藏文章
class UserCollectArticle(models.Model):
	iSerialNumberOfArticle = models.ForeignKey(Article, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)

	def __str__(self):
		return '%s 收藏 %s' % (self.iAccount, self.iSerialNumberOfArticle)

# 使用者按讚文章
class UserLikesArticle(models.Model):
	iSerialNumberOfArticle = models.ForeignKey(Article, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)

	def __str__(self):
		return '%s 按讚 %s' % (self.iAccount, self.iSerialNumberOfArticle)

# 使用者按讚留言
class UserLikesComment(models.Model):
	commentofArticle = models.ForeignKey(CommentOfArticle, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)

