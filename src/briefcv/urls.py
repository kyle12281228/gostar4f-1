from django.urls import path
from . import views
	
	#path('briefcv/', include('briefcv.urls')),
urlpatterns = [
	path('public', views.public_cv, name='public_cv'),
	path('mycv/', views.mybriefcv, name='mybriefcv'),
	path('mycv/new_mycv/', views.new_mycv, name='new_mycv'),
	path('mycv/<str:iSerialNumberOfBriefCv>/', views.briefcv_detail, name='briefcv detail'),
	path('mycv/<str:iSerialNumberOfBriefCv>/<str:iPageNumber>/', views.briefcv_pages, name='briefcv_page'),
	path('mycv/<str:iSerialNumberOfBriefCv>/<str:iPageNumber>/nextmould/<str:nextmould>', views.briefcv_pages_nextmould, name='briefcv_pages_nextmould'),

	path('mycv/<str:iSerialNumberOfBriefCv>/<str:iPageNumber>/newpage/', views.briefcv_newpage, name='briefcv_newpage'),

	path('output/<str:iSerialNumberOfBriefCv>/<str:iPageNumber>/', views.briefcv_output, name='briefcv_output'),
	#path('<str:sBoardName>/search/', views.search, name='search article'),
	#path('hashtag/<str:sTopic>/', views.hashtag, name='hashtag'),
	#path('<str:iSerialNumberOfBriefCv>/', views.briefcv_detail, name='briefcv detail'),
	#path('<str:sBoardName>/', views.board, name='specific board'),'''
]