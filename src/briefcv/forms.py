from django import forms
from briefcv.models import *
from django.forms import CharField, ModelMultipleChoiceField, ModelChoiceField

class BriefCvPageForm(forms.Form):
	iPageNumber = forms.IntegerField(label = '頁碼', required = False)
	iMouldNumber = forms.IntegerField(label = '模板', required = False)
	sTitle = forms.CharField(label = '標題', required = False, widget = forms.TextInput(attrs={'id' : 'title','placeholder':'標題'}))
	sContent = forms.CharField(label = '內文', required = False, widget = forms.TextInput(attrs={'id' : 'content','placeholder':'標題'}))
	sPictureURL = forms.CharField(label = '嵌入URL', required = False, widget = forms.Textarea(attrs={'class' : 'pictureURL','placeholder':'URL'}))